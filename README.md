% Informatique de Calandreta

---------------------------------------------------------------------

# Serveur

Installation d'une Debian Jessie 8.5 de base.

S'assurer qu'il a au moins deux disques en RAID-1 (miroir), pour se donner un
délai d'intervention, quand l'un des disques grillera, pour pouvoir s'en
procurer un nouveau et venir le remplacer.

Le premier utilisateur local créé doit être *calandreta* (UID = 1000), afin
d'être cohérent avec celui créé sur toutes les machines. Il doit être dans le
groupe *sudo*. Il faut créer sa paire de clés SSH (`ssh-keygen`) qui sera
utilisée pour les déploiements de configuration via Ansible.

Quelques opérations de base :

```bash
grub-install /dev/sdb

apt install sudo
adduser calandreta sudo

vim /etc/network/interfaces
```

---------------------------------------------------------------------

# Ansible

Ansible est un outils de déploiement et de gestion de configurations.

On l'utilise ici pour modifier les configurations des postes clients sans
avoir à le faire à la main sur chacun d'eux, et sans avoir à les réinstaller.

Préparatifs initiaux :

```bash
# Récupérer le logiciel Ansible :
cd
git clone https://github.com/ansible/ansible.git --recursive
sudo easy_install pip
sudo pip install paramiko PyYAML jinja2 httplib2 six markupsafe

# Récupérer le module cups_lpadmin qui n'est pas encore intégré dans Ansible :
cd ~/ansible/lib/ansible/modules/extras/system
wget https://raw.githubusercontent.com/k0ste/ansible-modules-extras/35473942e2f6a60476c1bb5af502783f5a077e55/system/cups_lpadmin.py

# Récupérer notre dépôt des configurations :
cd
git clone https://gitlab.com/MathieuMD/calandreta-toulouse.git

```

```bash
# Préparer l'environnement de travail Ansible
cd ~/calandreta-toulouse/ansible/
source source_me

# Pour reconfigurer tous les postes :
ansible-playbook -i inventory site.yml

# Pour ne reconfigurer qu'un seul poste, par exemple .123 :
ansible-playbook -i inventory site.yml -l 10.0.0.123

# Pour installer les logiciels en masse :
ansible-playbook -i inventory site.yml -t logiciels

# Éteindre tous les postes :
ansible -i inventory all -m command -a "shutdown -h now"
```

Pour ajouter des logicies, éditer le fichier `site.yml`, puis rejouer la
commande correspondante ci-dessus.

---------------------------------------------------------------------

# Passerelle internet

Le serveur fait office de passerelle vers internet :

```bash
echo "1" | sudo tee /proc/sys/net/ipv4/ip_forward

sudo vim /etc/sysctl.conf
# net.ipv4.ip_forward=1

sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

Ne pas oublier de sauver les règles du pare-feu ensuite (voir section
[Firewall](#firewall))

## DNS

Utiliser un résolveur de noms DNS local pour ne pas dépendre de celui d'un
tiers.

Installer le paquet `unbound`.

Créer le fichier `/etc/unbound/unbound.conf.d/server.conf` :

```
server:
    # Sur 10.0.0.2 :
    interface: 10.0.0.2
    interface: 127.0.0.1
    access-control: 10.0.0.0/24 allow
```

Puis le redémarrer : `sudo /etc/init.d/unbound restart`

## Miroir APT

Pour éviter de surcharger la connexion internet pour les installations et
mises à jour, il vaut mieux héberger les paquets Debian localement.

Prévoir au moins 200 Gio d'espace disque !

```bash
sudo apt install apt-mirror proftpd-basic
sudo vim /etc/apt/mirror.list
sudo apt-mirror
sudo crontab -e
# 0 1 * * *   /usr/bin/apt-mirror > /home/apt-mirror/mirror/archive.ubuntu.com/apt-mirror.log && /home/apt-mirror/var/clean.sh >> /home/apt-mirror/mirror/archive.ubuntu.com/apt-mirror.log
```

Configurer un accès anonyme en lecture seul dans
`/etc/proftpd/conf.d/anonymous.conf` :

```
<Anonymous ~ftp>
    User                ftp
    Group               nogroup
    UserAlias           anonymous ftp
    RequireValidShell   off
    MaxClients          10
    <Directory *>
        <Limit WRITE>
            DenyAll
        </Limit>
    </Directory>
</Anonymous>
```


## Proxy filtrant

Le filtrage de l'accès internet est obligatoire pour les établissement dès
lors qu'ils accueillent au moins un mineur.

Squid est le proxy utilisé pour filtrer l'accès internet, en utilisant [le
fichier liste noire](https://dsi.ut-capitole.fr/blacklists/) mis à disposition
par l'Université Toulouse 1.

Afin d'éviter de devoir configurer tous les clients du réseau pour qu'ils
utilisent le proxy explicitement, on force le passage par le proxy (on parle
de « proxy transparent »).

Le fichier `squidGuard.conf` doit être déposé sous `/etc/squidguard`.

```bash
sudo apt install squid3 squidguard apache2

# S'assurer que c'est bien l'interface eth1 qui est connectée au LAN
iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 80 -j REDIRECT --to-port 3128

sudo rsync -arpogvt rsync://ftp.ut-capitole.fr/blacklist/dest/ /var/lib/squidguard/db/
sudo squidGuard -d -C all
sudo update-squidguard

sudo crontab -e
# 0 2 * * *  rsync -arpogvt rsync://ftp.ut-capitole.fr/blacklist/dest/ /var/lib/squidguard/db/ ; squidGuard -C all ; update-squidguard

sudo squid3 -k reconfigure
```

Le fichier `block.html` doit être sous `/var/www/html`.

Note : les flux chiffrés HTTPS ne peuvent *pas* être filtrés (en tout cas, pas
sans effets secondaires).

## Firewall

```bash
sudo apt install netfilter-persistent

# Après chaque ouverture de port :
sudo /etc/init.d/netfilter-persistent save
```

## Exim

```bash
sudo dpkg-reconfigure exim4-config
```

## Fail2ban

```bash
sudo apt install fail2ban
sudo cp -a /etc/fail2ban/jail.{conf,local}
sudo vim /etc/fail2ban/jail.local
```

---------------------------------------------------------------------

# Démarrage des clients

Pour se simplifier la vie du parc informatique, on configure les PC clients
pour démarrer depuis une image fournie par le serveur (PXE boot) afin de les
installer automatiquement s'ils n'ont pas déjà été installés. S'ils sont déjà
installés, alors ils démarreront automatiquement sur le disque local (voir les
options de démarrage dans
[pxelinux.cfg/default](tftp/calandreta/pxelinux.cfg/default)).

## DHCP

Le protocole DHCP permet d'attribuer automatiquement une IP aux PC du réseau.

Installer le paquet `isc-dhcp-server`.

Spécifier l'interface réseau sur laquelle restreindre le service DHCP dans le
fichier `/etc/default/isc-dhcp-server`.

Redémarrer le serveur DHCP : `sudo /etc/init.d/isc-dhcp-server restart`

## TFTP

Le protocole TFTP permet de charger un noyau d'amorçage pour démarrer les PC
clients.

Installer le paquet `tftpd-hpa`.

Récupérer une image de boot réseau (netboot) Ubuntu 16.04 Xenial pour chaque
architectures 64-bit et 32-bit :

```bash
# 64-bit
sudo mkdir -p /srv/tftp/ubuntu-amd64
cd !$
wget http://archive.ubuntu.com/ubuntu/dists/xenial/main/installer-amd64/current/images/netboot/netboot.tar.gz | sudo tar -xvzf -
sudo chmod -R a+r .
cd ..
# 64-bit est le choix par défaut :
sudo ln -s ubuntu-amd64 ubuntu

# 32-bit
sudo mkdir -p /srv/tftp/ubuntu-i386
cd !$
wget http://archive.ubuntu.com/ubuntu/dists/xenial/main/installer-i386/current/images/netboot/netboot.tar.gz | sudo tar -xvzf -
sudo chmod -R a+r .
```

Ensuite copier le menu Syslinux personalisé pour Calandreta
`pxelinux.cfg/default` qui se trouve dans notre dépôt Git :

```bash
sudo cp -a ~/calandreta-toulouse/tftp/calandreta /srv/tftp/
```

Redémarrer le serveur TFTP : `sudo /etc/init.d/tftpd-hpa restart`

## FTP

Un serveur FTP basique sert [le fichier de
pré-réponses](ftp/preseed/preseed-xubuntu-calandreta.cfg) utilisé lors de
l'installation des PC clients, ainsi que [le script de
pré-configuration](ftp/preseed/postinstall-calandreta.sh) une
fois l'installation terminée.

Installer le paquet `proftpd-basic`.

Configurer un accès anonyme en lecture seule (voir plus haut, section [Miroir
APT](#miroir-apt)).

Copier les fichiers de notre dépôt Git :

```bash
sudo cp -a ~/calandreta-toulouse/ftp/* /srv/ftp/
```

S'assurer que le fichier `/srv/ftp/authorized_keys` contient bien la clé
publique SSH de l'utilisateur *calandreta*. Typiquement, le contenu de
`/home/calandreta/.ssh/id_rsa.pub` :

```bash
[ $(whoami) == "calandreta" -a ! -f ~/.ssh/id_rsa ] && ssh-keygen || echo "La biclé existe déjà."
cat ~/.ssh/id_rsa.pub > /srv/ftp/authorized_keys
```

---------------------------------------------------------------------

# Partage de fichiers

## NFS

```bash
sudo apt install nfs-kernel-server
# etc. ^^

```

## Samba

(abandonné, remplacé par NFS)

---------------------------------------------------------------------

# Authentification unifiée

## LDAP

Serveur OpenLDAP installé et configuré comme backend d'authentification Unix.

