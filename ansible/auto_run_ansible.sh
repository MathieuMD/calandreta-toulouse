#!/bin/bash
# Lancer Ansible régulièrement

BASE="/home/calandreta"
LOG="$BASE/$(basename $0).log"
ANSIBLE_PATH="$BASE/ansible"
PLAYBOOK_PATH="$BASE/calandreta-toulouse/ansible"
INVENTORY="inventory"
PLAYBOOK="site.yml"

echo "START @ $(date -Is)" >"$LOG"

cd $PLAYBOOK_PATH
source "$ANSIBLE_PATH/hacking/env-setup" -q

ansible-playbook -vvv -i "$INVENTORY" "$PLAYBOOK" >>"$LOG" 2>&1

echo "END @ $(date -Is)" >>"$LOG"
