<head>
<meta charset="utf-8" />
<title>Accès bloqué</title>
</head>
<body>

<p>
L'accès à cette adresse est bloqué par l'une des listes noires <a
href="https://dsi.ut-capitole.fr/blacklists/">Blacklists UT1</a> utilisées sur
le réseau du collège Calandreta.
</p>

<p>
Pour information :
</p>
<pre>
URL: <?php echo htmlentities($_GET['url'])."\n"; ?>
Target Group: <?php echo $_GET['targetgroup']."\n"; ?>
Client Address: <?php echo $_GET['clientaddr']."\n"; ?>
<?php
/*
Client Group: <?php echo $_GET['clientgroup']."\n"; ?>
Client Name: <?php echo $_GET['clientname']."\n"; ?>
Client User: <?php echo $_GET['clientuser']."\n"; ?>
*/
?>
</pre>

</body>
