#!/bin/bash -

# Préparer un utilisateur pour les déploiements automatiques via Ansible
apt-get -y install sudo
adduser --disabled-password --gecos "Ansible dedicated user" deploy
passwd -l deploy
mkdir -p -m 700 ~deploy/.ssh
chown deploy:deploy ~deploy/.ssh
wget ftp://10.0.0.1/authorized_keys -O - >> ~deploy/.ssh/authorized_keys
echo "deploy ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Ajouter l'utilisateur principal aux groupes
addgroup calandreta lpadmin
addgroup calandreta scanner
addgroup calandreta nopasswdlogin

# Langue par défaut : occitan
update-locale LANG=oc_FR.UTF-8 LANGUAGE=oc_FR:

# Login automatique
echo "[SeatDefaults]
autologin-user = calandreta
user-session = xubuntu
greeter-session = lightdm-gtk-greeter
autologin-user-timeout = 0
" > /etc/lightdm/lightdm.conf.d/20-autologin.conf

# Installer quelques paquets de base
apt-get -y install gimp gimp-help-fr gimp-data-extras \
    audacity libreoffice-impress scratch geogebra \
    flashplugin-installer marble

# Mais pour réellement compléter l'installation, il reste encore à jouer le
# playbook Ansible.
